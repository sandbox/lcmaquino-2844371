CONTENTS OF THIS FILE
=====================

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
============

This module allow administrators to add a "Share node" field to some content
type so the users can indicate who else has access to edit their nodes.

 * For a full description of the module, visit the project page:
   http://drupal.org/sandbox/lcmaquino/2844371

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2844371


REQUIREMENTS
============

No special requirements.


INSTALLATION
============
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
=============

 * Go to "Structure" > "Content types" and add a "Share node" field to the
   content type you want to enable this module.

 * Go to "Modules" > "Share Node" > "Settings" > "Field name to use
   Share node:" and paste the field name you added in the content type.

 * Go to "Modules" > "Share Node" > "Settings" > "Public keyword:" and set the
   public keyword that you want to use to represent when a node can be edited
   by any user.

For example:
 * We have a content type "Article";
 * "Article" has a field "Share" (with system name "field_share") of the type
   "Share node";
 * On "Modules" > "Share Node" > "Settings" > "Field name to use Share 
   node:" we type field_share;
 * On "Modules" > "Share Node" > "Settings" > "Public keyword:" we type 
   <public>;

If some user create an "Article" and set field "Share" to <public>, then any
other user can edit this node.

If some user create an "Article" and set field "Share" to "UserA, UserB, 
UserC", then only that user, UserA, UserB, and UserC can edit this node.

MAINTAINERS
===========

Current maintainer:
 * Luiz C. M. de Aquino (lcmaquino) - https://www.drupal.org/user/3514838
