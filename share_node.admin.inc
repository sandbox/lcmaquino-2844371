<?php

/**
 * @file
 * Create the module settings form.
 */

/**
 * Create the settings form.
 */
function share_node_admin_settings($form, &$form_state) {
  $form['share_node_field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Field name to use Share node:'),
    '#default_value' => variable_get('share_node_field_name', ''),
    '#description' => t('Enter the field name in your content type that will 
use Share node. You can add a Share node field to a content type at 
<a href="@content_types_url">Structure &raquo; Content types</a>.',
  array('@content_types_url' => url('admin/structure/types'))),
    '#size' => 20,
  );
  $form['share_node_public_keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('Public keyword:'),
    '#default_value' => variable_get('share_node_public_keyword', '<public>'),
    '#description' => t('Enter the keyword used to indicate that a node could 
be edited by any user.'),
    '#size' => 20,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Validate the settings form.
 */
function share_node_admin_settings_validate($form, &$form_state) {
  $field_name = filter_xss($form_state['values']['share_node_field_name'], array());
  $public_keyword = trim($form_state['values']['share_node_public_keyword']);
  $field_name_info = field_info_field($field_name);
  if (empty($public_keyword)) {
    form_set_error('share_node_public_keyword', t('The public keyword can not be empty.'));
  }
  if ($field_name_info) {
    if ($field_name_info['type'] != 'share_node_with') {
      form_set_error('share_node_field_name', t('The field "@field_name" should 
be of the type "Share node".', array('@field_name' => $field_name)));
    }
  }
  else {
    form_set_error('share_node_field_name', t('The field "@field_name" was not found.', array('@field_name' => $field_name)));
  }
}

/**
 * Submit handler for settings form.
 */
function share_node_admin_settings_submit($form, &$form_state) {
  variable_set('share_node_field_name', $form_state['values']['share_node_field_name']);
  variable_set('share_node_public_keyword', $form_state['values']['share_node_public_keyword']);
  drupal_set_message(t('Your settings have been saved successfully.'));
}
